package navarro.jarod;

import java.util.Objects;

public class Motor {
    private String serie;
    private String numCilindros;

    public Motor() {
    }

    public Motor(String serie, String numCilindros) {
        this.serie = serie;
        this.numCilindros = numCilindros;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getNumCilindros() {
        return numCilindros;
    }

    public void setNumCilindros(String numCilindros) {
        this.numCilindros = numCilindros;
    }

    @Override
    public String toString() {
        return
                "serie: " + serie +
                        ", numCilindros: " + numCilindros
                ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Motor motor = (Motor) o;
        return Objects.equals(serie, motor.serie) &&
                Objects.equals(numCilindros, motor.numCilindros);
    }

}
