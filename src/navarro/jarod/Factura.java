package navarro.jarod;
import java.util.*;

public class Factura {
    private String numero;
    private String cliente;
    private ArrayList<Linea>lineas = new ArrayList<Linea>();
    private Fecha fechaFactura;

    public Factura() {
    }

    public Factura(String numero, String cliente, Fecha fechaFactura) {
        this.numero = numero;
        this.cliente = cliente;
        this.fechaFactura = fechaFactura;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public ArrayList<Linea> getLineas() {
        return lineas;
    }

    public void agregarLinea(Linea lineas) {
        this.lineas.add(lineas);
    }

    public Fecha getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(Fecha fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    @Override
    public String toString() {
        return
                "numero: " + numero +
                        ", cliente: " + cliente +
                        ", lineas: " + lineas +
                        ", fechaFactura: " + fechaFactura
                ;
    }
}
