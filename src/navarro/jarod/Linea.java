package navarro.jarod;

import java.util.Objects;

public class Linea {
    private int cantidad;
    private String codigo;
    private String descripcion;
    private double precio;

    public Linea() {
    }

    public Linea(int cantidad, String codigo, String descripcion, double precio) {
        this.cantidad = cantidad;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.precio = precio*cantidad;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio*getCantidad();
    }

    @Override
    public String toString() {
        return
                "cantidad: " + cantidad +
                        ", codigo: " + codigo +
                        ", descripcion: " + descripcion +
                        ", precio: " + precio
                ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Linea linea = (Linea) o;
        return cantidad == linea.cantidad &&
                Double.compare(linea.precio, precio) == 0 &&
                Objects.equals(codigo, linea.codigo) &&
                Objects.equals(descripcion, linea.descripcion);
    }
}
