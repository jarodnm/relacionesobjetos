package navarro.jarod;

import java.util.Objects;

public class Empleado {
    private String mnombre;
    private String mcedula;

    public Empleado() {
       this.mnombre = " ";
       this.mcedula = " ";

    }

    public Empleado(String mnombre, String mcedula) {
        this.mnombre = mnombre;
        this.mcedula = mcedula;
    }

    public String getMnombre() {
        return mnombre;
    }

    public void setMnombre(String mnombre) {
        this.mnombre = mnombre;
    }

    public String getMcedula() {
        return mcedula;
    }

    public void setMcedula(String mcedula) {
        this.mcedula = mcedula;
    }

    @Override
    public String toString() {
        return
                "mnombre: " + mnombre +
                        ", mcedula: " + mcedula
                ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Empleado empleado = (Empleado) o;
        return Objects.equals(mnombre, empleado.mnombre) &&
                Objects.equals(mcedula, empleado.mcedula);
    }

}
