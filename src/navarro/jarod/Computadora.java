package navarro.jarod;

import java.util.Objects;

public class Computadora {
    private String mnumSerie;
    private String mmarca;
    Empleado mresponable;

    public Computadora() {
        this.mnumSerie = " ";
        this.mmarca = " ";
    }

    public Computadora(String mnumSerie, String mmarca) {
        this.mnumSerie = mnumSerie;
        this.mmarca = mmarca;
    }

    public Computadora(String mnumSerie, String mmarca, Empleado mresponable) {
        this.mnumSerie = mnumSerie;
        this.mmarca = mmarca;
        this.mresponable = mresponable;
    }

    public String getMnumSerie() {
        return mnumSerie;
    }

    public void setMnumSerie(String mnumSerie) {
        this.mnumSerie = mnumSerie;
    }

    public String getMmarca() {
        return mmarca;
    }

    public void setMmarca(String mmarca) {
        this.mmarca = mmarca;
    }

    public Empleado getMresponable() {
        return mresponable;
    }

    public void setMresponable(Empleado mresponable) {
        this.mresponable = mresponable;
    }

    @Override
    public String toString() {
        return
                "mnumSerie: " + mnumSerie +
                        ", mmarca: " + mmarca +
                        ", mresponable: " + mresponable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Computadora that = (Computadora) o;
        return Objects.equals(mnumSerie, that.mnumSerie) &&
                Objects.equals(mmarca, that.mmarca) &&
                Objects.equals(mresponable, that.mresponable);
    }

}
