package navarro.jarod;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.io.*;
import java.util.*;

public class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static ArrayList<Empleado> empleados = new ArrayList<>();
    static ArrayList<Computadora> computadoras = new ArrayList<>();
    static ArrayList<Motor> motores = new ArrayList<>();
    static ArrayList<Vehiculo> vehiculos = new ArrayList<>();


    public static void main(String[] args) throws IOException {
        boolean noSalir = true;
        int opcion;
        do {
            out.println("Digite su opción");
            out.println("--------Asoción--------");
            out.println("1.Registrar Empleado");
            out.println("2.Registrar computadora");
            out.println("3.Listar Empleados");
            out.println("4.Listar computadoras");
            out.println("------Agregación-------");
            out.println("5.Registrar Vehiculo");
            out.println("6.Registrar Motor");
            out.println("7.Listar Vehiculos");
            out.println("8.Listar Motores");
            out.println("------Composición------");
            out.println("9.Generar Factura");
            out.println("10.Salir");
            opcion = Integer.parseInt(in.readLine());

            switch (opcion) {
                case 1:
                    registrarEmpleado();
                    break;
                case 2:
                    registrarComputadora();
                    break;
                case 3:
                    listarEmpleados();
                    break;
                case 4:
                    listarComputadoras();
                    break;
                case 5:
                    registrarVehiculo();
                    break;
                case 6:
                    registrarMotor();
                    break;
                case 7:
                    listarVehiculos();
                    break;
                case 8:
                    listarMotores();
                    break;
                case 9:
                    generarFactura();
                    break;
                case 10:
                    noSalir = false;
                    out.println("Gracias por usar el programa");
                    break;
                default:
                    out.println("Digite una opcion valida");
                    break;
            }
        } while (noSalir);
    }

    public static void registrarEmpleado() throws IOException {
        String mnombre;
        String mcedula;
        boolean existe = false;
        out.println("Digite el nombre");
        mnombre = in.readLine();
        out.println("Digite la cedula");
        mcedula = in.readLine();
        Empleado tmpempleado = new Empleado(mnombre, mcedula);
        for (Empleado dato : empleados) {
            if (dato.equals(tmpempleado)) {
                existe = true;
            }
        }

        if (existe) {
            out.println("El Empleado ya existe");
        } else {
            out.println("Empleado registrado con exito");
            empleados.add(tmpempleado);
        }
    }

    public static void registrarComputadora() throws IOException {
        String mnumSerie;
        String mmarca;
        String nombre;
        String cedula;
        boolean existe = false;
        out.println("Digite el numero de serie");
        mnumSerie = in.readLine();
        out.println("Digite la marca");
        mmarca = in.readLine();
        out.println("Digite el nombre del responsable asignado");
        nombre = in.readLine();
        out.println("Digite la cedula del responsable asignado");
        cedula = in.readLine();
        Empleado tmpempleado = new Empleado(nombre, cedula);
        for (Empleado dato : empleados) {
            if (dato.equals(tmpempleado)) {
                existe = true;
            }
        }
        if (existe) {
            Computadora tmpcomputadora = new Computadora(mnumSerie, mmarca, tmpempleado);
            computadoras.add(tmpcomputadora);

        } else {
            out.println("El empleado digitado no existe");
        }
    }

    public static void listarEmpleados() {
        for (Empleado dato : empleados) {
            out.println(dato.toString());
        }
    }

    public static void listarComputadoras() {
        for (Computadora dato : computadoras) {
            out.println(dato.toString());
        }
    }

    public static void registrarMotor() throws IOException {
        String serie;
        String numCilindros;
        boolean existe = false;
        out.println("Digite el numero de serie");
        serie = in.readLine();
        out.println("Digite la cantidad de cilindros");
        numCilindros = in.readLine();
        Motor tmpmotor = new Motor(serie, numCilindros);
        for (Motor dato : motores) {
            if (dato.equals(tmpmotor)) {
                existe = true;
            }
        }
        if (existe) {
            out.println("El motor ya existe");
        } else {
            out.println("Motor registrado correctamente");
            motores.add(tmpmotor);
        }
    }

    public static void registrarVehiculo() throws IOException {
        String numSerieVehiculo;
        String marca;
        String numCilindros;
        boolean motorExiste = false;
        out.println("Digite el numero de serie del vehiculo");
        numSerieVehiculo = in.readLine();
        out.println("Digite la marca");
        marca = in.readLine();
        out.println("Digite la cantidad de cilindros del motor");
        numCilindros = in.readLine();
        Motor tmpmotor = new Motor(numSerieVehiculo, numCilindros);
        for (Motor dato : motores) {
            if (dato.equals(tmpmotor)) {
                motorExiste = true;
            }
        }

        if (motorExiste) {
            Vehiculo tmpvehiculo = new Vehiculo(numSerieVehiculo, marca, tmpmotor);
            vehiculos.add(tmpvehiculo);
            out.println("El Vehiculo fue registrado satisfactoriamente");
        } else {
            out.println("El motor no esta registrado");
        }

    }

    public static void listarMotores() {
        for (Motor dato : motores) {
            out.println(dato.toString());
        }
    }

    public static void listarVehiculos() {
        for (Vehiculo dato : vehiculos) {
            out.println(dato.toString());
        }
    }

    public static void generarFactura() throws IOException {
        String numero;
        String cliente;
        int dia;
        int mes;
        int annio;
        Fecha fechaFactura;
        int cantidad;
        String codigo;
        String descripcion;
        double precio;
        String des;
        out.println("Digite el numero de factura");
        numero = in.readLine();
        out.println("Digite el nombre del cliente");
        cliente = in.readLine();
        out.println("Digite el día");
        dia = Integer.parseInt(in.readLine());
        out.println("Digite el mes");
        mes = Integer.parseInt(in.readLine());
        out.println("Digite el año");
        annio = Integer.parseInt(in.readLine());
        fechaFactura = new Fecha(dia, mes, annio);
        Factura tmpfactura = new Factura(numero, cliente, fechaFactura);
        do {
            out.println("Digite la cantidad de producto");
            cantidad = Integer.parseInt(in.readLine());
            out.println("Digite el codigo de producto");
            codigo = in.readLine();
            out.println("Digite la descripcion del producto");
            descripcion = in.readLine();
            out.println("Digite el precio del producto");
            precio = Double.parseDouble(in.readLine());
            Linea tmplinea = new Linea(cantidad, codigo, descripcion, precio);
            tmpfactura.agregarLinea(tmplinea);
            out.println("Digite 1 para continuar o cualquier cosa para salir");
            des = in.readLine();
        } while (des.equals("1"));
        out.println(tmpfactura.toString());

    }
}
