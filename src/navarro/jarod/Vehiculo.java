package navarro.jarod;

import java.util.Objects;

public class Vehiculo {
    private String numSerieVehiculo;
    private String marca;
    private Motor nmotor;

    public Vehiculo() {
    }

    public Vehiculo(String numSerieVehiculo, String marca, String numCilindros) {
        this.numSerieVehiculo = numSerieVehiculo;
        this.marca = marca;
        this.nmotor = new Motor(numSerieVehiculo,numCilindros);
    }

    public Vehiculo(String numSerieVehiculo, String marca, Motor nmotor) {
        this.numSerieVehiculo = numSerieVehiculo;
        this.marca = marca;
        this.nmotor = nmotor;
    }

    public String getNumSerieVehiculo() {
        return numSerieVehiculo;
    }

    public void setNumSerieVehiculo(String numSerieVehiculo) {
        this.numSerieVehiculo = numSerieVehiculo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Motor getNmotor() {
        return nmotor;
    }

    public void setNmotor(Motor nmotor) {
        this.nmotor = nmotor;
    }

    @Override
    public String toString() {
        return
                "numSerieVehiculo: " + numSerieVehiculo +
                        ", marca: " + marca +
                        ", nmotor: " + nmotor
                ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehiculo vehiculo = (Vehiculo) o;
        return Objects.equals(numSerieVehiculo, vehiculo.numSerieVehiculo) &&
                Objects.equals(marca, vehiculo.marca) &&
                Objects.equals(nmotor, vehiculo.nmotor);
    }


}
